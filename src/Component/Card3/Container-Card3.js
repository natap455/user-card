import styled from 'styled-components'

const ContainerComponent = styled.div`
    background: linear-gradient(90deg,#fff,#ece9e6);
    width: 1280px;
    min-height: 100vh;
    margin: 0px auto 0;
    margin-bottom:30px;
    padding-top:30px;
    padding-bottom:50px;
    display: grid;
    place-content: center;
    justify-content: center;
    align-content: center;
    grid-template-columns: 350px 350px 350px;//1fr 1fr 1fr
    grid-template-rows: auto;
    border-bottom-left-radius: 20px;
    border-bottom-right-radius: 20px;
    grid-gap: 50px;
`

export const ContainerCard3 = (props)=>{
    return(
        <ContainerComponent>
                {props.children}
        </ContainerComponent>
    )
}
