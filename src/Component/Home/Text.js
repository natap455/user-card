import styled from 'styled-components'

const TextComponent = styled.h2`
    color: ${props => props.color};
    font-size: ${props => props.fontSize};
    &:hover{
        background-color: aliceblue;
    }
`
export const Text = (props)=>{
    return(
        <TextComponent 
            fontSize={props.fontSize} 
            color={props.color}>
            {props.text}
        </TextComponent>
    )
}