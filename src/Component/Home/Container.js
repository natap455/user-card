import styled from 'styled-components'

const ContainerComponent = styled.div`
    background: linear-gradient(90deg,#fff,#ece9e6);
    width: 1280px;
    min-height: 80vh;
    margin: 0px auto 0;
    //padding-top:30px;
    display: grid;
    place-content: center;
    justify-content: center;
    align-content: center;
    grid-template-columns: 300px 300px 300px;//1fr 1fr 1fr
    grid-template-rows: auto;
    border-bottom-left-radius: 20px;
    border-bottom-right-radius: 20px;
    //grid-gap: 50px;
`

export const Container = (props)=>{
    return(
        <ContainerComponent>
                {props.children}
        </ContainerComponent>
    )
}
