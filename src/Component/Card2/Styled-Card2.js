import styled from 'styled-components'

export const CardContainerComponent = styled.div`
    width: 17.375em; // 400px 
	height: 20.5em; // 600px 
	position: relative;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
    margin-bottom:0px;
`

export const FrontContainerComponent = styled.div`
    width: 100%;
	height: 100%;
	position: absolute;
	left: 0;
	top: 0;
	box-shadow: 2px 3px 5px black;
	overflow: hidden;
    border-radius: 34% 32% 42% 41% / 19% 20% 68% 70% ;
`

export const ImgBoxContainerComponent = styled.div`
    width: 100%;
	height: 60%;
	text-align: center;
	background: #085c9c;
	position: relative;
	border-top-left-radius: 15px;
	border-top-right-radius: 15px;
    &:after {
        content: "";
        width: 50px;//50
        height: 100%;
        position: absolute;
        top: 0;
        right: -50%;
        background: rgba(255,255,255,0.3);
        transform: skewX(30deg);
        transition: right 1s;
    }
    ${FrontContainerComponent}:hover &:after{
        right: 130%;
        transition-delay: 0.5s;
    }
`
export const ImgComponent = styled.img`
    padding-top: 30px;//10
    width:130px;
	color: #fff;
	transition: all 1s;
    ${FrontContainerComponent}:hover &{
        transform: scale(1.1);
        color: #2E5EAA;
    }
`

export const PComponent = styled.p`
    margin: 0px;
	text-align: center;
	color: #085c9c;
    font-size: ${props => props.fontSize};
    font-family: ${props => props.fontFamily};
    font-weight: ${props => props.fontWeight};
`
export const H1Component = styled.h1`
    color: black;
    visibility: hidden;
`

export const LegendContainerComponent = styled.div`
    width: 100%;
	height: 40%;
	position: absolute;
    background-color: #c9cad1;
    display: flex;
    justify-content: center;
    &:after {
	content: "";
	width: 100%;
	height: 100%;
	position: absolute;
	top: 0;
	left: 0;
	background: linear-gradient(-45deg,
	            rgba(255,255,255,0.4),
		          transparent
	);
}
`

export const ContLiComponent = styled.div`
    width: 100%;
    height: auto;
    display:grid;
    align-items: center;
    justify-content: row;
    margin: ${props => props.margin};
    word-break: ${props => props.wordbreak};
`

export const LiComponent=styled.li`
    list-style: none;
    width: ${props => props.width}; // 150px
    text-align: center;
    margin-top: -5px;
    margin-left: ${props => props.marginLeft};// 50px
`

export const ShadowContainerComponent = styled.div`
    border-radius: 39% 12% 11% 39% / 50% 12% 11% 50% ;
    width: 70px;
	height: auto;
	position: absolute;
	top: 10px;
	right: -75px;
	display: flex;
	flex-direction: column;
	justify-content: space-around;
	perspective: 600px;
	transform-origin: left center;
	transform: rotateY(90deg);
	transition: all 1s;
	z-index: -1;
    ${CardContainerComponent}:hover &{
        transform: rotateY(0deg);
    }
`

export const AShadowComponent = styled.a`
border-radius: 39% 12% 11% 39% / 50% 12% 11% 50% ;
border-color: #085c9c;
    border-style: solid;
    text-decoration: none;
    margin-bottom: 2px;
	width: 100%;
	height: 55px;
	line-height: 55px;
	background: white;
	text-align: center;
	border-bottom-right-radius: 10px;
	transform: rotateY(90deg);
	transition: all 0.35s;
	transform-origin: left center;
    ${CardContainerComponent}:hover &{
        transform: rotateY(0deg);
    }
    &:hover:nth-child(1){background: #c9cad1;}
    &:hover:nth-child(2){background: #c9cad1;}
    &:hover:nth-child(3){background: #c9cad1;}
    &:hover:nth-child(4){background: #c9cad1;}
`