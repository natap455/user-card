import styled from 'styled-components'
import intento1 from '../../Images/intento1.jpg';
import intento2 from '../../Images/intento2.jpg';
import intento3 from '../../Images/intento3.jpg';
import face from '../../Images/facebook.png';
import gmail from '../../Images/gmail.png';
import linked from '../../Images/linked.png';

const UserContainerComponent = styled.div`
    position:relative;
    width:200px;
    height:auto;
    margin:15px auto 0;
    display:grid;
    grid-template-columns:1fr 1fr 1fr;
    grid-template-rows:440px;
    grid-gap:40px;
    
`
const CardContainerComponent = styled.div`
    width:250px;
    height:300px;
    position:relative;
    background:#CCC;
    border-radius:10px;
    transition:.5s;
    overflow:hidden;
    &:hover{
        transform:translatey(-20px);//rotateY(180deg)
        box-shadow:0 20px 20px rgba(0,0,0,.2);
        background-color: #dcdde1;
    }
    &:nth-child(1){
        background:linear-gradient(0deg,#c21833,transparent);
    }
    &:nth-child(2){
        background:linear-gradient(0deg,#8012a5,transparent);
    }
    &:nth-child(3){
        background:linear-gradient(0deg,#3a414c,transparent);
    }
    
`
const ImgContainerComponent = styled.img`
    width:100%;
    height:auto;
`
const ImgIconContainerComponent = styled.img`
    width:20px;
    height:20px;
`

const ImgBoxContainerComponent = styled.div`
    position:absolute;
    top:0;
    left:0;
    bottom:0;
    right:0;
    &:hover{
        opacity:0;
    }
`

const H2Component = styled.h2`
    color: blueviolet;
    margin:0 0 10px;
    padding:0;
    font-size:20px;
    text-align:center;
`

const SpanComponent = styled.span`
    font-size:16px;
    color: orangered;
    text-align:center;
`

const PComponent = styled.p`
    font-size:16px;
    color:#000000;
    margin:0;
    padding:0;
    text-align:center;
`

//Problema
/*display:grid;
    grid-template-columns:1fr 1fr 1fr;
    grid-template-rows: auto;*/

const ULComponent = styled.ul`
    display:flex;
    align-items:center;
    justify-content:center;
    margin:20px 0 0;
    padding:0;
    list-style:none;
`
const ContentContainerComponent = styled.div`
    bottom:-100%;
    left:0;
    text-align:center;
    padding:20px;
    padding-top:80px;
    width:100%;
    height:60%;
    box-sizing:border-box;
    transition:.5s;
`
const AComponent = styled.a`
    color:#FFF;
    font-size:18px;
    padding:0 10px;
`





export const UserCard = ()=>{
    return(
        <>
        <UserContainerComponent>
            <CardContainerComponent>
                <ImgBoxContainerComponent>
                    <ImgContainerComponent src={intento1} width="100%" alt="intento1"/>
                </ImgBoxContainerComponent>
                <ContentContainerComponent>
                    <H2Component>someone famous<br/><SpanComponent>UX/UI Desginer</SpanComponent></H2Component>
                    <PComponent>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</PComponent>
                    <ULComponent>
                        <li><AComponent><ImgIconContainerComponent src={face} alt="face"/></AComponent></li>
                        <li><AComponent><ImgIconContainerComponent src={gmail} alt="gmail"/></AComponent></li>
                        <li><AComponent><ImgIconContainerComponent src={linked} alt="linked"/></AComponent></li>
                    </ULComponent>
                </ContentContainerComponent>
            </CardContainerComponent>
        </UserContainerComponent>

        <UserContainerComponent>
            <CardContainerComponent>
                <ImgBoxContainerComponent>
                    <ImgContainerComponent src={intento2} width="100%" alt="intento2"/>
                </ImgBoxContainerComponent>
                <ContentContainerComponent>
                    <H2Component>someone famous<br/><SpanComponent>UX/UI Desginer</SpanComponent></H2Component>
                    <PComponent>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</PComponent>
                    <ULComponent>
                        <li><AComponent><ImgIconContainerComponent src={face} alt="face"/></AComponent></li>
                        <li><AComponent><ImgIconContainerComponent src={gmail} alt="gmail"/></AComponent></li>
                        <li><AComponent><ImgIconContainerComponent src={linked} alt="linked"/></AComponent></li>
                    </ULComponent>
                </ContentContainerComponent>
            </CardContainerComponent>
        </UserContainerComponent>

        <UserContainerComponent>
            <CardContainerComponent>
                <ImgBoxContainerComponent>
                    <ImgContainerComponent src={intento3} width="100%" alt="intento3"/>
                </ImgBoxContainerComponent>
                <ContentContainerComponent>
                    <H2Component>someone famous<br/><SpanComponent>UX/UI Desginer</SpanComponent></H2Component>
                    <PComponent>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</PComponent>
                    <ULComponent>
                        <li><AComponent><ImgIconContainerComponent src={face} alt="face"/></AComponent></li>
                        <li><AComponent><ImgIconContainerComponent src={gmail} alt="gmail"/></AComponent></li>
                        <li><AComponent><ImgIconContainerComponent src={linked} alt="linked"/></AComponent></li>
                    </ULComponent>
                </ContentContainerComponent>
            </CardContainerComponent>
        </UserContainerComponent>

        <UserContainerComponent>
            <CardContainerComponent>
                <ImgBoxContainerComponent>
                    <ImgContainerComponent src={intento1} width="100%" alt="intento1"/>
                </ImgBoxContainerComponent>
                <ContentContainerComponent>
                    <H2Component>someone famous<br/><SpanComponent>UX/UI Desginer</SpanComponent></H2Component>
                    <PComponent>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</PComponent>
                    <ULComponent>
                        <li><AComponent><ImgIconContainerComponent src={face} alt="face"/></AComponent></li>
                        <li><AComponent><ImgIconContainerComponent src={gmail} alt="gmail"/></AComponent></li>
                        <li><AComponent><ImgIconContainerComponent src={linked} alt="linked"/></AComponent></li>
                    </ULComponent>
                </ContentContainerComponent>
            </CardContainerComponent>
        </UserContainerComponent>

        <UserContainerComponent>
            <CardContainerComponent>
                <ImgBoxContainerComponent>
                    <ImgContainerComponent src={intento2} width="100%" alt="intento2"/>
                </ImgBoxContainerComponent>
                <ContentContainerComponent>
                    <H2Component>someone famous<br/><SpanComponent>UX/UI Desginer</SpanComponent></H2Component>
                    <PComponent>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</PComponent>
                    <ULComponent>
                        <li><AComponent><ImgIconContainerComponent src={face} alt="face"/></AComponent></li>
                        <li><AComponent><ImgIconContainerComponent src={gmail} alt="gmail"/></AComponent></li>
                        <li><AComponent><ImgIconContainerComponent src={linked} alt="linked"/></AComponent></li>
                    </ULComponent>
                </ContentContainerComponent>
            </CardContainerComponent>
        </UserContainerComponent>

        <UserContainerComponent>
            <CardContainerComponent>
                <ImgBoxContainerComponent>
                    <ImgContainerComponent src={intento3} width="100%" alt="intento3"/>
                </ImgBoxContainerComponent>
                <ContentContainerComponent>
                    <H2Component>someone famous<br/><SpanComponent>UX/UI Desginer</SpanComponent></H2Component>
                    <PComponent>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</PComponent>
                    <ULComponent>
                        <li><AComponent><ImgIconContainerComponent src={face} alt="face"/></AComponent></li>
                        <li><AComponent><ImgIconContainerComponent src={gmail} alt="gmail"/></AComponent></li>
                        <li><AComponent><ImgIconContainerComponent src={linked} alt="linked"/></AComponent></li>
                    </ULComponent>
                </ContentContainerComponent>
            </CardContainerComponent>
        </UserContainerComponent>

        </>
    )
}

// Configuracion personalizada de las tarjetas

    /*background-color: black;
    width: 250px;
    height: 250px;
    margin: 20px auto 0;
    display: grid;
    place-content: center;
    justify-content: center;
    align-content: center;
    grid-template-columns: 300px 300px 300px;
    grid-template-rows: auto;
    border-radius: 30px;*/



//--------------------------------------------------------------------
//CODIGO DE CODEPEN




