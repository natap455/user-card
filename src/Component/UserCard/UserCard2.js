import { ImgComponent, FrontContainerComponent, ImgBoxContainerComponent, PComponent, LegendContainerComponent, ShadowContainerComponent, AShadowComponent, CardContainerComponent, LiComponent, ContLiComponent, H1Component } from "../Card2/Styled-Card2";
import React, { useState } from "react"
import { data3 } from '../Card2/Data';

import Redes from '../../Images/Redes2.svg';
import InfoPersona from '../../Images/InfoPersona2.svg';
import InfoProfesionalimg from '../../Images/InfoProfesional2.svg';
import InfoEstudios from '../../Images/InfoEstudios2.svg';

const data = [
    {
        id:0,//#7EB9BB  #5A7787 #15A187           #15A187
        icon:InfoPersona,
        //Informacion:"name",
        //Informacion2:"cargo",
        TipofontSize:" 1.1em",
        TipofontSize2:" 0.9em",
        TipoColor:"#046e7a",
        TipoColor2:"#025059",
        TipoNegrilla:"bold",
        MarginLi:"35px 0px 30px 0px",
        separar:"none",
        width1:'200px', 
        marginLeft1:'14%',
        width2:'190px', 
        marginLeft2:'15%',
        info:data3.map((datainfo)=>{return(<div>{datainfo.name}</div>)}),
        info2:data3.map((datainfo)=>{return(<div>{datainfo.cargo}</div>)}),
    },
    {
        id:1,
        icon:InfoProfesionalimg, 
        //Informacion:"InfoProfesional",
        //Informacion2:"InfoPersona",
        TipofontSize:" 1.2em",
        TipofontSize2:" 0em",
        TipoColor:"#893237",
        TipoColor2:"pink",
        TipoNegrilla:"bold",
        MarginLi:"20px 0px 30px 0px",
        separar:"none",
        width1:'200px', 
        marginLeft1:'14%',
        width2:'190px', 
        marginLeft2:'15%',
        info:data3.map((datainfo)=>( <div key={datainfo.id}>{datainfo.InfoProfesional}</div>)),
        info2:data3.map((datainfo)=>{return(<div>{datainfo.InfoPersona}</div>)}),
        
    },
    {
        id:2,
        icon:Redes, 
        Informacion:"mail",
        Informacion2:"cvlac",
        TipofontSize:" 0.9em",
        TipofontSize2:" 0.8em",
        TipoColor:"#b55808",
        TipoColor2:"#b55808",
        MarginLi:"20px 0px 30px 0px",
        separar:"break-all",
        width1:'220px', 
        marginLeft1:'10.5%',
        width2:'190px', 
        marginLeft2:'15%',
        
    },
    {
        id:3,
        icon:InfoEstudios, 
        Informacion:"InfoEstudios",
        Informacion2:"InfoPersona",
        TipofontSize:" 0.9em",
        TipofontSize2:" 0.3em",
        TipoColor:"#c8e9ed",
        TipoColor2:"transparent",
        TipoNegrilla:"bold",
        MarginLi:"20px 0px 30px 0px",
        separar:"none",
        width1:'200px', 
        marginLeft1:'14%',
        width2:'190px', 
        marginLeft2:'15%',
    }
        ];

export const Content3 = (props) => {
    const [Count, setCount]=useState(0);

    const InfObtenida =data.find( Contador => Contador.id === Count );
    const InfObtenida2 =data3.find( IdB => IdB.id === props.id );
    
    switch(Count){
        case 0:
            data[Count].Informacion=(InfObtenida2.name);
            data[Count].Informacion2=(InfObtenida2.InfoPersona);
        break;
        case 1:
            data[Count].Informacion=(InfObtenida2.cargo);
            data[Count].Informacion2=(InfObtenida2.InfoPersona);
        break;
        case 2:
            data[Count].Informacion=(InfObtenida2.mail);
            data[Count].Informacion2=(InfObtenida2.cvlac);
        break;
        case 3:
            data[Count].Informacion=(InfObtenida2.InfoEstudios);
            data[Count].Informacion2=(InfObtenida2.InfoPersona);
        break;
        default:
            data[0].Informacion=(InfObtenida2.name);
            data[0].Informacion2=(InfObtenida2.cargo);
        break;
    }
    
    return (
        <CardContainerComponent>
            <H1Component>{props.id}</H1Component>
            <FrontContainerComponent>
                <ImgBoxContainerComponent>
                    <ImgComponent src={props.ImgPersona}/>
                </ImgBoxContainerComponent>
                <LegendContainerComponent>
                <ContLiComponent 
                        margin={InfObtenida.MarginLi} 
                        wordbreak={InfObtenida.separar}> 
                        <LiComponent 
                            width={InfObtenida.width1} 
                            marginLeft={InfObtenida.marginLeft1}>
                            <PComponent 
                                fontSize={InfObtenida.TipofontSize} 
                                fontFamily='Montserrat, sans-serif;' 
                                color={InfObtenida.TipoColor} 
                                fontWeight={InfObtenida.TipoNegrilla}>
                                {InfObtenida.Informacion}
                            </PComponent>
                        </LiComponent>
                        <LiComponent 
                            width={InfObtenida.width2} 
                            marginLeft={InfObtenida.marginLeft2}>
                            <PComponent 
                                fontSize={InfObtenida.TipofontSize2} 
                                fontFamily='Montserrat, sans-serif;' 
                                color={InfObtenida.TipoColor2}>
                                {InfObtenida.Informacion2}
                            </PComponent>
                        </LiComponent>
                    </ContLiComponent>
                </LegendContainerComponent>
            </FrontContainerComponent>
            <ShadowContainerComponent>
                {
                    data.map((dataRead)=>(
                            <AShadowComponent
                                key={dataRead.id}
                                onClick={()=> setCount(dataRead.id)}>
                                <img src={dataRead.icon} width="20" alt="icono"/>
                            </AShadowComponent>
                    ))
                }
            </ShadowContainerComponent>
        </CardContainerComponent>
    )
}

/* 
                    {
                    data.map((dataDatosCard)=>(
                            <ContLiComponent
                                key={dataDatosCard.id}>
                                    <LiComponent 
                                        width={InfObtenida.width1} 
                                        marginLeft={InfObtenida.marginLeft1}>
                                        <PComponent 
                                            fontSize={InfObtenida.TipofontSize} 
                                            fontFamily='Montserrat, sans-serif;' 
                                            color={InfObtenida.TipoColor} 
                                            fontWeight={InfObtenida.TipoNegrilla}>
                                            {dataDatosCard.info}
                                        </PComponent>
                                    </LiComponent>
                                    <LiComponent 
                                        width={InfObtenida.width2} 
                                        marginLeft={InfObtenida.marginLeft2}>
                                        <PComponent 
                                            fontSize={InfObtenida.TipofontSize2} 
                                            fontFamily='Montserrat, sans-serif;' 
                                            color={InfObtenida.TipoColor2}>
                                            {dataDatosCard.info2}
                                        </PComponent>
                                    </LiComponent>
                            </ContLiComponent>
                    ))
                } */