import { CardContainerComponent, ContentContainerComponent, H2Component, ImgBoxContainerComponent, ImgContainerComponent, PComponent, SpanComponent, ULComponent } from "../Card1/Styled-Card1";
import face from '../../Images/facebook.png';
import gmail from '../../Images/gmail.png';
import linked from '../../Images/linked.png';

export const Content = (props) => {
    return (
        <CardContainerComponent>
            <ImgBoxContainerComponent>
                <ImgContainerComponent src={props.image} width="100%" alt="intento2"/>
            </ImgBoxContainerComponent>
            <ContentContainerComponent>
                <H2Component>{props.name}</H2Component>   
                <SpanComponent>{props.cargo}</SpanComponent>
                <PComponent>{props.descripcion}</PComponent>
                <ULComponent>
                    <li><a href="https://www.youtube.com/"><ImgContainerComponent src={face} alt="face" width='20px' height='20px'/></a></li>
                    <li><a href={props.mailref}><ImgContainerComponent src={gmail} alt="gmail" width='20px' height='20px'/></a></li>
                    <li><a href={props.linkedinref}><ImgContainerComponent src={linked} alt="linked" width='20px' height='20px'/></a></li>
                </ULComponent>
            </ContentContainerComponent>
        </CardContainerComponent>
    )
}





