import styled from 'styled-components'

export const CardContainerComponent = styled.div`
    width:250px;
    height:300px;
    position:relative;
    background:#CCC;
    border-radius:10px;
    transition:.5s;
    overflow:hidden;
    padding:20px;
    background:linear-gradient(0deg,#5B8390,transparent);
    &:hover{
        transform:translatey(-20px) ;//rotateY(180deg)
        box-shadow:0 20px 20px rgba(0,0,0,.2);
        background-color: #dcdde1;
    }
    
`
export const ImgBoxContainerComponent = styled.div`
    position:absolute;
    top:0;
    left:0;
    bottom:0;
    right:0;
    &:hover{
        opacity:0;
    }
`

export const ContentContainerComponent = styled.div`
    bottom:-100%;
    left:0;
    text-align:center;
    padding-top:80px;
    width:100%;
    height:60%;
    box-sizing:border-box;
    transition:.5s;
    
`

export const ImgContainerComponent = styled.img`
    width: ${props => props.width};
    height: ${props => props.height};
    alt: ${props => props.alt};
`

export const H2Component = styled.h2`
    color: #033242;
    margin:0 0 10px;
    padding:0;
    font-size:20px;
    text-align:center;
`

export const SpanComponent = styled.span`
    font-size:16px;
    color: #203942;
    text-align:center;
`

export const PComponent = styled.p`
    font-size:16px;
    color:#3F2642;
    margin:0;
    padding:0;
    text-align:center;
`

export const ULComponent = styled.ul`
    display:grid;
    grid-template-columns:1fr 1fr 1fr;
    grid-template-rows: auto;
    align-items:center;
    justify-content:center;
    margin:20px 50px 0;
    padding:0;
    list-style:none;
`