import styled from "styled-components"
import {NavLink as Link} from 'react-router-dom'
import logo from '../../Images/logo_upn_cidet.svg';

const NavbarContainerComponent = styled.div`
    background: linear-gradient(90deg,#fff,#ece9e6);
    width: 1280px;
    display: grid;
    align-content: center;
    justify-content: space-between;
    grid-template-areas: "LogoUpn NavMenu .";
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    margin: 20px auto 0;
`
const NavMenuContainerComponent = styled.div`
    grid-area: NavMenu;
    list-style-type: none;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    grid-template-rows: 1fr;
    grid-auto-flow: column;
    justify-self: center;
    align-items: center;
    margin-top: 20px;
`
const LogoNavComponent = styled.div`
    grid-area: LogoUpn;
    width: 320px;
    margin-left: 30px;
    margin-top: 20px;
`

const NavItemComponent = styled.li`
    font-size: 20px;
    font-weight: 550;
    color: #09456d;
    text-decoration: none;
    text-align: center;
`

const NavLink=styled(Link)`
    text-decoration: none;
`

export const Navbar = () => {
    return (
        <NavbarContainerComponent>
            <LogoNavComponent>
                <img src={logo} width="320" alt="logo"/>
            </LogoNavComponent>
            <NavMenuContainerComponent>
                <NavLink to='/contact'>
                    <NavItemComponent>Podcast</NavItemComponent>
                </NavLink>
                <NavLink to='/Card3'>
                    <NavItemComponent>Productos</NavItemComponent>
                </NavLink>
                <NavLink to='/Card2'>
                    <NavItemComponent>Servicios</NavItemComponent>
                </NavLink>
                <NavLink to='Card1'>
                    <NavItemComponent>Equipo Cinndet</NavItemComponent>
                </NavLink>
                <NavLink to='/'>
                    <NavItemComponent>Moodle</NavItemComponent>
                </NavLink>
            </NavMenuContainerComponent>
        </NavbarContainerComponent>
    )
}
