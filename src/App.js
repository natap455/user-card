//import logo from './logo.svg';
//import './App.css';
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom'
import { Navbar } from './Component/Navbar/Navbar'
import { Contact } from './Page/Contact'
import { Home } from './Page/Home'
import { Card2 } from './Page/Card2'
import { Card3 } from './Page/Card3'
import { Card1 } from './Page/Card1'

//
export const App = ()=> {
  return (
    <>
      <BrowserRouter>
      <Navbar/>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route exact path='/contact' component={Contact}/>
          <Route exact path='/Card1' component={Card1}/>
          <Route exact path='/Card2' component={Card2}/>
          <Route exact path='/Card3' component={Card3}/>
          <Redirect to='/'/>
        </Switch>
      </BrowserRouter>
    </>
  );
}