import { ContainerCard3 } from "../Component/Card3/Container-Card3"
import { data3 } from "../Component/Card3/Data"
import { Content3 } from "../Component/UserCard/UserCard3"



//import logo from '../Images/logo_upn_cidet.svg';
//import { Text } from "../Component/Home/Text"


export const Card3 = () => {
    return (
        <ContainerCard3>
            {data3.map((persona, index) => {
                return(
                    <Content3
                        key={index}
                        id={persona.id}
                        ImgPersona={persona.ImgPersona}
                        name={persona.name}
                        cargo={persona.cargo}
                        mail={persona.mail}
                        cvlac={persona.cvlac}
                        InfoPersona={persona.InfoPersona}
                        InfoProfesional={persona.InfoProfesional}
                        InfoEstudios={persona.InfoEstudios}
                    />
                )
            })
            }
        </ContainerCard3>
    )
}