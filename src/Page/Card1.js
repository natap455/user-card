import { Container } from "../Component/Card1/Container-Card1"
import { Content } from "../Component/UserCard/UserCard1"

import intento1 from '../Images/intento1.jpg';
import intento2 from '../Images/intento2.jpg';
import intento3 from '../Images/intento3.jpg';


//import logo from '../Images/logo_upn_cidet.svg';
//import { Text } from "../Component/Home/Text"

export const data = [
    {
        id:0,
        ImgPersona: intento1,
        name:'Carlos Hernán López Ruiz', 
        cargo: ' Coordinador General',
        descripcion:'clopez@pedagogica.edu.co',
        //faceref:'https://www.facebook.com/nathalia.pinzonsaiz',
        mailref:'natap455@gmail.com',
        linkedinref:'https://www.linkedin.com/in/nata-pinz-281513170/',
    },
    {
        id:1,
        ImgPersona: intento2,
        name:'Ligia Lozano Cifuentes',
        cargo:'Coordinadora Acividades Tecnológicas',
        descripcion:'lclozano@pedagogica.edu.co',
        faceref:'@',
        mailref:'@',
        linkedinref:'@',
    },
    {
        id:2,
        ImgPersona: intento3,
        name:'Jorge Leonador Hernández Rozo',
        cargo:'Desarrollador Web',
        descripcion:'jlhernandezr@pedagogica.edu.co',
        faceref:'@',
        mailref:'@',
        linkedinref:'@',
    },
    {
        id:3,
        ImgPersona: intento2,
        name:'Johann Mateo Soler López',
        cargo:'Diseñador E-learning',
        descripcion:'jmsolerl@upn.edu.co',
        faceref:'@',
        mailref:'@',
        linkedinref:'@',
    },
    {
        id:4,
        ImgPersona: intento2,
        name:'Diana Marcela Sánchez Yáñez',
        cargo:'Administradora Plataforma LMS',
        descripcion:'dmsanchezy@pedagogica.edu.co',
        faceref:'@',
        mailref:'@',
        linkedinref:'@',
    },
    {
        id:5,
        ImgPersona: intento2,
        name:'Jhonny Alexander Ortegón Moreno',
        cargo:'Gestor de Contenidos',
        descripcion:'dqu_jaortegonm361@pedagogica.edu.co',
        faceref:'@',
        mailref:'@',
        linkedinref:'@',
    },
    {
        id:6,
        ImgPersona: intento2,
        name:'Sandra Castañeda Valdés ',
        cargo:'Secretaria',
        descripcion:'castaned@pedagogica.edu.co',
        faceref:'@',
        mailref:'@',
        linkedinref:'@',
    },
    {
        id:7,
        ImgPersona: intento2,
        name:'Camila Prontón Leguizamón ',
        cargo:'Asisten Administrativa',
        descripcion:'cpontonl@upn.edu.co',
        faceref:'@',
        mailref:'@',
        linkedinref:'@',
    }

];

export const Card1 = () => {
    return (
        <Container>
            {data.map((persona, index) => {
                return(
                    <div key={index}>
                    <Content
                        image={persona.ImgPersona}
                        name={persona.name}
                        cargo={persona.cargo}
                        descripcion={persona.descripcion}
                        //faceref={persona.faceref}
                        mailref={persona.mailref}
                        linkedinref={persona.linkedinref}
                    />
                    </div>
                )
            })
            }
        </Container>
    )
}